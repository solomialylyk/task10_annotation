package model;

import annotation.MyField;

public class MyClass {
    @MyField(name = "Hello")
    private int integerValue;
    @MyField
    private String name;
    private double doubleValue;

    private void getInfo() {
        System.out.println("Hello from Solomia");
    }

    public String[] myMethodstring(String str, String... args) {
        String[] strings = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            strings[i] = str + " -> " + args[i] + " -> " + i;
        }
        return strings;
    }

    public int getIntegerValue() {
        return integerValue;
    }

    public void setIntegerValue(int integerValue) {
        this.integerValue = integerValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDoubleValue() {
        return doubleValue;
    }

    public void setDoubleValue(double doubleValue) {
        this.doubleValue = doubleValue;
    }

    @Override
    public String toString() {
        return "MyClass{" +
                "integerValue=" + integerValue +
                ", name='" + name + '\'' +
                ", doubleValue=" + doubleValue +
                '}';
    }
}
