package view;

import annotation.MyField;
import model.MyClass;
import model.MyGenericClass;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - test fields with annotation");
        menu.put("2", "  2 - test unknown object");
        menu.put("3", "  3 - found Washbsins wuth price < 3500");
        menu.put("4", "  4 - found by name Door");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton4() {
        MyClass myClass = new MyClass();
        Class clazz = myClass.getClass();
        String[] strings = {"one", "two", "three"};
        try {
            Method method =clazz.getDeclaredMethod("myMethodstring", new Class[] {String.class, String[].class });
            method.setAccessible(true);
            String[] strings1 =(String[])method.invoke(myClass, "hi", strings);
            for (String str : strings1) {
                System.out.println(str);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }

    private void pressButton3() {
        MyGenericClass<MyClass> obj = new MyGenericClass<>(MyClass.class);

    }

    private void pressButton2() {
        MyClass myClass = new MyClass();
        reflUnknownObject(myClass);
    }

    void reflUnknownObject(Object object) {
        try {
            Class clazz = object.getClass();
            System.out.println("The name of class is: " + clazz.getSimpleName());
            Constructor constructor = clazz.getConstructor();
            System.out.println("The name of constructor: " + constructor.getName());
            System.out.println("The methods of class are: ");
            Method[] methods = clazz.getMethods();
            for (Method method : methods) {
                System.out.println(method.getName());
            }
            System.out.println();
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                System.out.println(field.getName());
            }
            fields[0].setAccessible(true);
            if (fields[0].getType() == int.class) {
                fields[0].setInt(object, 47);
            }
            System.out.println(object);
            Method methodCall = clazz.getDeclaredMethod("getInfo");
            methodCall.setAccessible(true);
            methodCall.invoke(object);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void pressButton1() {
        Class clazz = MyClass.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyField.class)) {
                MyField myField = (MyField) field.getAnnotation(MyField.class);
                System.out.println(" Field " + field.getName());
                System.out.println(" name in @MyField " + myField.name());
                System.out.println(" age in @MyField " + myField.age());
            }
        }

    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).printGoods();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

}
